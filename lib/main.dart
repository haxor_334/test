import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedcoverwidget1/GeneratedCoverWidget1.dart';
import 'package:flutterapp/halloapp/generatedcoverwidget2/GeneratedCoverWidget2.dart';
import 'package:flutterapp/halloapp/generatedrectangle2widget/GeneratedRectangle2Widget.dart';
import 'package:flutterapp/halloapp/generatedmockupwidget1/GeneratedMockupWidget1.dart';
import 'package:flutterapp/halloapp/generatedsplashscreenwidget/GeneratedSplashScreenWidget.dart';
import 'package:flutterapp/halloapp/generatedsigninwidget/GeneratedSignInWidget.dart';
import 'package:flutterapp/halloapp/generatedsignupwidget/GeneratedSignupWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/GeneratedHomeWidget.dart';
import 'package:flutterapp/halloapp/generatedsearchwidget1/GeneratedSearchWidget1.dart';
import 'package:flutterapp/halloapp/generatedsavedrecipewidget/GeneratedSavedRecipeWidget.dart';
import 'package:flutterapp/halloapp/generatednotificationsallwidget/GeneratedNotificationsAllWidget.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/GeneratedProfileWidget3.dart';
import 'package:flutterapp/halloapp/generatednotificationsreadwidget/GeneratedNotificationsReadWidget.dart';
import 'package:flutterapp/halloapp/generatednotificationsreadwidget1/GeneratedNotificationsReadWidget1.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/GeneratedNotificationsUnreadWidget.dart';
import 'package:flutterapp/halloapp/generatedframe48widget/GeneratedFrame48Widget.dart';
import 'package:flutterapp/halloapp/generatedsearchresultwidget/GeneratedSearchResultWidget.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/GeneratedFilterWidget.dart';
import 'package:flutterapp/halloapp/generatedcomponent5widget2/GeneratedComponent5Widget2.dart';
import 'package:flutterapp/halloapp/generatedcomponent6widget1/GeneratedComponent6Widget1.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/GeneratedRecipeIngridentWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentmorewidget/GeneratedRecipeIngridentMoreWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/GeneratedRecipeIngridentShareWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/GeneratedRecipeIngridentsharelinkcopiedWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/GeneratedRecipeIngridentRateRecipeWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/GeneratedRecipeIngridentRaterecipeWidget.dart';
import 'package:flutterapp/halloapp/generatedactionlistwidget1/GeneratedActionlistWidget1.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/GeneratedRecipeProcedureWidget.dart';
import 'package:flutterapp/halloapp/generatedpopwidget2/GeneratedPopWidget2.dart';
import 'package:flutterapp/halloapp/generatedimage1widget/GeneratedImage1Widget.dart';
import 'package:flutterapp/halloapp/generatedimage2widget/GeneratedImage2Widget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/GeneratedColorsWidget.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/GeneratedIconsWidget3.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/GeneratedTypographyWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/GeneratedRecipeIngridentWidget1.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/GeneratedRecipeProcedureWidget1.dart';

void main() {
  runApp(halloApp());
}

class halloApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/GeneratedCoverWidget1',
      routes: {
        '/GeneratedCoverWidget1': (context) => GeneratedCoverWidget1(),
        '/GeneratedCoverWidget2': (context) => GeneratedCoverWidget2(),
        '/GeneratedRectangle2Widget': (context) => GeneratedRectangle2Widget(),
        '/GeneratedMockupWidget1': (context) => GeneratedMockupWidget1(),
        '/GeneratedSplashScreenWidget': (context) =>
            GeneratedSplashScreenWidget(),
        '/GeneratedSignInWidget': (context) => GeneratedSignInWidget(),
        '/GeneratedSignupWidget': (context) => GeneratedSignupWidget(),
        '/GeneratedHomeWidget': (context) => GeneratedHomeWidget(),
        '/GeneratedSearchWidget1': (context) => GeneratedSearchWidget1(),
        '/GeneratedSavedRecipeWidget': (context) =>
            GeneratedSavedRecipeWidget(),
        '/GeneratedNotificationsAllWidget': (context) =>
            GeneratedNotificationsAllWidget(),
        '/GeneratedProfileWidget3': (context) => GeneratedProfileWidget3(),
        '/GeneratedNotificationsReadWidget': (context) =>
            GeneratedNotificationsReadWidget(),
        '/GeneratedNotificationsReadWidget1': (context) =>
            GeneratedNotificationsReadWidget1(),
        '/GeneratedNotificationsUnreadWidget': (context) =>
            GeneratedNotificationsUnreadWidget(),
        '/GeneratedFrame48Widget': (context) => GeneratedFrame48Widget(),
        '/GeneratedSearchResultWidget': (context) =>
            GeneratedSearchResultWidget(),
        '/GeneratedFilterWidget': (context) => GeneratedFilterWidget(),
        '/GeneratedComponent5Widget2': (context) =>
            GeneratedComponent5Widget2(),
        '/GeneratedComponent6Widget1': (context) =>
            GeneratedComponent6Widget1(),
        '/GeneratedRecipeIngridentWidget': (context) =>
            GeneratedRecipeIngridentWidget(),
        '/GeneratedRecipeIngridentMoreWidget': (context) =>
            GeneratedRecipeIngridentMoreWidget(),
        '/GeneratedRecipeIngridentShareWidget': (context) =>
            GeneratedRecipeIngridentShareWidget(),
        '/GeneratedRecipeIngridentsharelinkcopiedWidget': (context) =>
            GeneratedRecipeIngridentsharelinkcopiedWidget(),
        '/GeneratedRecipeIngridentRateRecipeWidget': (context) =>
            GeneratedRecipeIngridentRateRecipeWidget(),
        '/GeneratedRecipeIngridentRaterecipeWidget': (context) =>
            GeneratedRecipeIngridentRaterecipeWidget(),
        '/GeneratedActionlistWidget1': (context) =>
            GeneratedActionlistWidget1(),
        '/GeneratedRecipeProcedureWidget': (context) =>
            GeneratedRecipeProcedureWidget(),
        '/GeneratedPopWidget2': (context) => GeneratedPopWidget2(),
        '/GeneratedImage1Widget': (context) => GeneratedImage1Widget(),
        '/GeneratedImage2Widget': (context) => GeneratedImage2Widget(),
        '/GeneratedColorsWidget': (context) => GeneratedColorsWidget(),
        '/GeneratedIconsWidget3': (context) => GeneratedIconsWidget3(),
        '/GeneratedTypographyWidget': (context) => GeneratedTypographyWidget(),
        '/GeneratedRecipeIngridentWidget1': (context) =>
            GeneratedRecipeIngridentWidget1(),
        '/GeneratedRecipeProcedureWidget1': (context) =>
            GeneratedRecipeProcedureWidget1(),
      },
    );
  }
}
