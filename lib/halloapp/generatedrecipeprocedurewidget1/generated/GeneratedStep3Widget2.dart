import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedLabelWidget258.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedBgWidget240.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedStep3Widget3.dart';

/* Group Step 3
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedStep3Widget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 93.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 93.0,
              child: GeneratedBgWidget240(),
            ),
            Positioned(
              left: 15.0,
              top: 32.0,
              right: null,
              bottom: null,
              width: 287.0,
              height: 53.0,
              child: GeneratedLabelWidget258(),
            ),
            Positioned(
              left: 15.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 19.0,
              child: GeneratedStep3Widget3(),
            )
          ]),
    );
  }
}
