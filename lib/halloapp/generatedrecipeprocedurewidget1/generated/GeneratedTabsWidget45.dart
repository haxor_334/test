import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedLabelWidget268.dart';

/* Frame Tabs
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTabsWidget45 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 107.0,
      height: 33.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                color: Color.fromARGB(255, 17, 148, 117),
              ),
            ),
            Positioned(
              left: 12.0,
              top: 8.0,
              right: null,
              bottom: null,
              width: 88.0,
              height: 22.0,
              child: GeneratedLabelWidget268(),
            )
          ]),
    );
  }
}
