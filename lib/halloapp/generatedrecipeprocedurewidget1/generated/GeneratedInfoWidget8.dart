import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedLocationWidget9.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget1/generated/GeneratedLabelWidget254.dart';

/* Group Info
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedInfoWidget8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 95.0,
      height: 40.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 23.0,
              right: null,
              bottom: null,
              width: 95.0,
              height: 17.0,
              child: GeneratedLocationWidget9(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 92.0,
              height: 23.0,
              child: GeneratedLabelWidget254(),
            )
          ]),
    );
  }
}
