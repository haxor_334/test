import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedTimeWidget67.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedFoodtitleWidget37.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedImageWidget106.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedRatingWidget44.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedReviewsWidget7.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedRectangle644Widget31.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget1/generated/GeneratedBookmarkWidget20.dart';

/* Group Card
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCardWidget36 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 201.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedImageWidget106(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle644Widget31(),
            ),
            Positioned(
              left: 268.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 16.0,
              child: GeneratedRatingWidget44(),
            ),
            Positioned(
              left: 5.0,
              top: 160.0,
              right: null,
              bottom: null,
              width: 196.0,
              height: 43.0,
              child: GeneratedFoodtitleWidget37(),
            ),
            Positioned(
              left: 284.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 24.0,
              height: 24.0,
              child: GeneratedBookmarkWidget20(),
            ),
            Positioned(
              left: 214.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 60.0,
              height: 24.0,
              child: GeneratedTimeWidget67(),
            ),
            Positioned(
              left: 217.0,
              top: 160.0,
              right: null,
              bottom: null,
              width: 93.0,
              height: 20.0,
              child: GeneratedReviewsWidget7(),
            )
          ]),
    );
  }
}
