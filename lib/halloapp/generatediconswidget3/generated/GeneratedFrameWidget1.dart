import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget570.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget568.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget569.dart';

/* Group frame
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFrameWidget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24.0,
      height: 24.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3958333333333333;
                double scaleX = (constraints.maxWidth * percentWidth) / 9.5;

                double percentHeight = 0.3954166571299235;
                double scaleY =
                    (constraints.maxHeight * percentHeight) / 9.489999771118164;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.3020833333333333,
                      translateY: constraints.maxHeight * 0.08333333333333333,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget568())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.5866667032241821;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 14.080000877380371;

                double percentHeight = 0.3852083683013916;
                double scaleY =
                    (constraints.maxHeight * percentHeight) / 9.245000839233398;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.20666666825612387,
                      translateY: constraints.maxHeight * 0.531458298365275,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget569())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 1.0;
                double scaleX = (constraints.maxWidth * percentWidth) / 24.0;

                double percentHeight = 1.0;
                double scaleY = (constraints.maxHeight * percentHeight) / 24.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 1.0,
                      translateY: constraints.maxHeight * 1.0,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget570())
                ]);
              }),
            )
          ]),
    );
  }
}
