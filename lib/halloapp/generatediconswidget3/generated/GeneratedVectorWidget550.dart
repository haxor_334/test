import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget550 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 19.0,
      height: 20.0,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M18 8.19L15.11 8.19C12.74 8.19 10.81 6.26 10.81 3.89L10.81 1C10.81 0.45 10.36 0 9.81 0L5.57 0C2.49 0 0 2 0 5.57L0 14.43C0 18 2.49 20 5.57 20L13.43 20C16.51 20 19 18 19 14.43L19 9.19C19 8.64 18.55 8.19 18 8.19ZM9.98 13.7C9.46 15.37 7.63 16.27 7 16.27C6.36 16.27 4.57 15.4 4.02 13.7C3.66 12.59 4.07 11.14 5.34 10.73C5.92 10.54 6.54 10.65 6.99 11C7.44 10.65 8.06 10.54 8.65 10.73C9.93 11.14 10.33 12.59 9.98 13.7Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
