import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget505 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 6.979999542236328,
      height: 1.4000000953674316,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M6.28 1.4L0.7 1.4C0.31 1.4 0 1.09 0 0.7C0 0.31 0.31 0 0.7 0L6.28 0C6.67 0 6.98 0.31 6.98 0.7C6.98 1.09 6.67 1.4 6.28 1.4Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
