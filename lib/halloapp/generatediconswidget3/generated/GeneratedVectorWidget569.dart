import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget569 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 14.080000877380371,
      height: 9.245000839233398,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M12.12 1.395C9.33 -0.465 4.78 -0.465 1.97 1.395C0.7 2.245 0 3.395 0 4.625C0 5.855 0.7 6.995 1.96 7.835C3.36 8.775 5.2 9.245 7.04 9.245C8.88 9.245 10.72 8.775 12.12 7.835C13.38 6.985 14.08 5.845 14.08 4.605C14.07 3.375 13.38 2.235 12.12 1.395Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
