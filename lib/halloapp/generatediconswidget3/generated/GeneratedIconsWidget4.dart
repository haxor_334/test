import 'package:flutter/material.dart';

/* Text Icons
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedIconsWidget4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Icons''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.1111111111111112,
        fontSize: 72.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 255, 255, 255),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
