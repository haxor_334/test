import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedDividerWidget16.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedIconsWidget4.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedOutlineBoldWidget.dart';

/* Frame font and examples
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFontandexamplesWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.zero,
      child: Container(
        width: 360.0,
        height: 136.0,
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              Positioned(
                left: 0.0,
                top: 0.0,
                right: null,
                bottom: null,
                width: 360.0,
                height: 0.0,
                child: GeneratedDividerWidget16(),
              ),
              Positioned(
                left: 0.0,
                top: 16.0,
                right: null,
                bottom: null,
                width: 202.0,
                height: 85.0,
                child: GeneratedIconsWidget4(),
              ),
              Positioned(
                left: 0.0,
                top: 112.0,
                right: null,
                bottom: null,
                width: 362.0,
                height: 26.0,
                child: GeneratedOutlineBoldWidget(),
              )
            ]),
      ),
    );
  }
}
