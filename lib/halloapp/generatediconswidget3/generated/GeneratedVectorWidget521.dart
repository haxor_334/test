import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget521 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 6.239999771118164,
      height: 6.240000247955322,
      child: SvgWidget(painters: [
        SvgPathPainter.stroke(
          1.5,
          strokeJoin: StrokeJoin.miter,
        )
          ..addPath(
              'M5.49 3.12C5.49 4.42892 4.42891 5.49 3.12 5.49L3.12 6.99C5.25734 6.99 6.99 5.25734 6.99 3.12L5.49 3.12ZM3.12 5.49C1.81108 5.49 0.75 4.42892 0.75 3.12L-0.75 3.12C-0.75 5.25734 0.982658 6.99 3.12 6.99L3.12 5.49ZM0.75 3.12C0.75 1.81109 1.81109 0.75 3.12 0.75L3.12 -0.75C0.982658 -0.75 -0.75 0.982659 -0.75 3.12L0.75 3.12ZM3.12 0.75C4.42891 0.75 5.49 1.81109 5.49 3.12L6.99 3.12C6.99 0.982659 5.25734 -0.75 3.12 -0.75L3.12 0.75Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
