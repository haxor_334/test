import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget499 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 19.0,
      height: 19.000001907348633,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M9.5 19C4.26 19 0 14.74 0 9.5C0 4.26 4.26 0 9.5 0C14.74 0 19 4.26 19 9.5C19 14.74 14.74 19 9.5 19ZM9.5 1.5C5.09 1.5 1.5 5.09 1.5 9.5C1.5 13.91 5.09 17.5 9.5 17.5C13.91 17.5 17.5 13.91 17.5 9.5C17.5 5.09 13.91 1.5 9.5 1.5Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
