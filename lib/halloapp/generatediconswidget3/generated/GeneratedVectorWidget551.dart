import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget551 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 6.803070545196533,
      height: 6.804855823516846,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M2.75 6.79486C3.7 6.80486 5.02 6.80486 6.15 6.80486C6.72 6.80486 7.02 6.13486 6.62 5.73486C5.18 4.28486 2.6 1.67486 1.12 0.194856C0.71 -0.215144 0 0.0648564 0 0.634856L0 4.12486C3.55271e-15 5.58486 1.24 6.79486 2.75 6.79486Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
