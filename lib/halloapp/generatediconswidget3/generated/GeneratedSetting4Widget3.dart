import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget493.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget497.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget494.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget492.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget496.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget498.dart';
import 'package:flutterapp/halloapp/generatediconswidget3/generated/GeneratedVectorWidget495.dart';

/* Group setting-4
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedSetting4Widget3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24.0,
      height: 24.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3125;
                double scaleX = (constraints.maxWidth * percentWidth) / 7.5;

                double percentHeight = 0.0625;
                double scaleY = (constraints.maxHeight * percentHeight) / 1.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.6354167064030966,
                      translateY: constraints.maxHeight * 0.23958335320154825,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget492())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.22916664679845175;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 5.499999523162842;

                double percentHeight = 0.0625;
                double scaleY = (constraints.maxHeight * percentHeight) / 1.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.05208333830038706,
                      translateY: constraints.maxHeight * 0.23958335320154825,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget493())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3541666666666667;
                double scaleX = (constraints.maxWidth * percentWidth) / 8.5;

                double percentHeight = 0.3541666666666667;
                double scaleY = (constraints.maxHeight * percentHeight) / 8.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.23958335320154825,
                      translateY: constraints.maxHeight * 0.09375,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget494())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.22916664679845175;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 5.499999523162842;

                double percentHeight = 0.0625;
                double scaleY = (constraints.maxHeight * percentHeight) / 1.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.71875,
                      translateY: constraints.maxHeight * 0.6979166666666666,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget495())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3125;
                double scaleX = (constraints.maxWidth * percentWidth) / 7.5;

                double percentHeight = 0.0625;
                double scaleY = (constraints.maxHeight * percentHeight) / 1.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.05208333830038706,
                      translateY: constraints.maxHeight * 0.6979166666666666,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget496())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3541666666666667;
                double scaleX = (constraints.maxWidth * percentWidth) / 8.5;

                double percentHeight = 0.3541666666666667;
                double scaleY = (constraints.maxHeight * percentHeight) / 8.5;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.40625,
                      translateY: constraints.maxHeight * 0.5520832935969034,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget497())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 1.0;
                double scaleX = (constraints.maxWidth * percentWidth) / 24.0;

                double percentHeight = 1.0;
                double scaleY = (constraints.maxHeight * percentHeight) / 24.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: 0,
                      translateY: 0,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget498())
                ]);
              }),
            )
          ]),
    );
  }
}
