import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget560 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 0.9979966282844543,
      height: 1.0,
      child: SvgWidget(painters: [
        SvgPathPainter.stroke(
          2.0,
          strokeCap: StrokeCap.round,
          strokeJoin: StrokeJoin.miter,
        )
          ..addPath(
              'M0.494507 -0.5C-0.0577774 -0.5 -0.505493 -0.0522848 -0.505493 0.5C-0.505493 1.05228 -0.0577774 1.5 0.494507 1.5L0.494507 -0.5ZM0.503489 1.5C1.05577 1.5 1.50349 1.05228 1.50349 0.5C1.50349 -0.0522848 1.05577 -0.5 0.503489 -0.5L0.503489 1.5ZM0.494507 1.5L0.503489 1.5L0.503489 -0.5L0.494507 -0.5L0.494507 1.5Z')
          ..color = Color.fromARGB(255, 121, 121, 121),
      ]),
    );
  }
}
