import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedTimeWidget27.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedCreatorWidget17.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRatingWidget22.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedFoodtitleWidget14.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedBookmarkWidget9.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRectangle646Widget12.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRectangle644Widget12.dart';

/* Group Card
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCardWidget17 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 150.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle646Widget12(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle644Widget12(),
            ),
            Positioned(
              left: 281.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 24.0,
              height: 24.0,
              child: GeneratedBookmarkWidget9(),
            ),
            Positioned(
              left: 268.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 16.0,
              child: GeneratedRatingWidget22(),
            ),
            Positioned(
              left: 10.0,
              top: 86.0,
              right: null,
              bottom: null,
              width: 202.0,
              height: 44.0,
              child: GeneratedFoodtitleWidget14(),
            ),
            Positioned(
              left: 10.0,
              top: 128.0,
              right: null,
              bottom: null,
              width: 55.0,
              height: 14.0,
              child: GeneratedCreatorWidget17(),
            ),
            Positioned(
              left: 211.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 60.0,
              height: 24.0,
              child: GeneratedTimeWidget27(),
            )
          ]),
    );
  }
}
