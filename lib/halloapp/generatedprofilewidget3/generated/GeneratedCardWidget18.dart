import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedFoodtitleWidget15.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedCreatorWidget18.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRectangle646Widget13.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedTimeWidget28.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedBookmarkWidget10.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRatingWidget23.dart';
import 'package:flutterapp/halloapp/generatedprofilewidget3/generated/GeneratedRectangle644Widget13.dart';

/* Group Card
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCardWidget18 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 150.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle646Widget13(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle644Widget13(),
            ),
            Positioned(
              left: 211.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 60.0,
              height: 24.0,
              child: GeneratedTimeWidget28(),
            ),
            Positioned(
              left: 281.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 24.0,
              height: 24.0,
              child: GeneratedBookmarkWidget10(),
            ),
            Positioned(
              left: 268.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 16.0,
              child: GeneratedRatingWidget23(),
            ),
            Positioned(
              left: 10.0,
              top: 91.0,
              right: null,
              bottom: null,
              width: 132.0,
              height: 39.0,
              child: GeneratedFoodtitleWidget15(),
            ),
            Positioned(
              left: 10.0,
              top: 128.0,
              right: null,
              bottom: null,
              width: 59.0,
              height: 14.0,
              child: GeneratedCreatorWidget18(),
            )
          ]),
    );
  }
}
