import 'package:flutter/material.dart';

/* Text Bio
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedBioWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: RichText(
          overflow: TextOverflow.visible,
          textAlign: TextAlign.left,
          text: const TextSpan(
            style: TextStyle(
              height: 1.171875,
              fontSize: 11.0,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
              color: Color.fromARGB(255, 121, 121, 121),

              /* letterSpacing: 0.0, */
            ),
            children: [
              TextSpan(
                text: '''Private Chef
Passionate about food and life 🥘''',
              ),
              TextSpan(
                text: '''���''',
                style: TextStyle(
                  height: 1.171875,
                  fontSize: 11.0,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(255, 121, 121, 121),

                  /* letterSpacing: null, */
                ),
              ),
              TextSpan(
                text: '''������''',
              ),
              TextSpan(
                text: '''���🍱
Mor''',
                style: TextStyle(
                  color: Color.fromARGB(255, 113, 177, 161),

                  /* letterSpacing: null, */
                ),
              )
            ],
          )),
    );
  }
}
