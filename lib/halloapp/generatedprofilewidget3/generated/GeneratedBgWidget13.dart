import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Bg
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedBgWidget13 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375.0,
      height: 106.0,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(20, 108, 108, 108),
            offset: Offset(0.0, 0.0),
            blurRadius: 8.0,
          )
        ],
      ),
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M0 0L133.125 0C142.872 0 151.129 7.14391 155.145 16.0252C160.344 27.5251 170.228 41 188 41C205.772 41 215.656 27.5251 220.855 16.0252C224.871 7.14391 233.128 0 242.875 0L375 0L375 106L0 106L0 0Z')
          ..color = Color.fromARGB(255, 255, 255, 255),
      ]),
    );
  }
}
