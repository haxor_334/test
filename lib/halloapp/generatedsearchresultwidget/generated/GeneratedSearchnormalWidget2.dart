import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatedsearchresultwidget/generated/GeneratedVectorWidget285.dart';
import 'package:flutterapp/halloapp/generatedsearchresultwidget/generated/GeneratedVectorWidget286.dart';
import 'package:flutterapp/halloapp/generatedsearchresultwidget/generated/GeneratedVectorWidget287.dart';

/* Group search-normal
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedSearchnormalWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 18.0,
      height: 18.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.8541668256123861;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 15.37500286102295;

                double percentHeight = 0.8541668256123861;
                double scaleY =
                    (constraints.maxHeight * percentHeight) / 15.37500286102295;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.052083326710595026,
                      translateY: constraints.maxHeight * 0.052083326710595026,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget285())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.14562503496805826;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 2.621250629425049;

                double percentHeight = 0.1457291841506958;
                double scaleY = (constraints.maxHeight * percentHeight) /
                    2.6231253147125244;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.8021874957614474,
                      translateY: constraints.maxHeight * 0.8021874957614474,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget286())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 1.0;
                double scaleX = (constraints.maxWidth * percentWidth) / 18.0;

                double percentHeight = 1.0;
                double scaleY = (constraints.maxHeight * percentHeight) / 18.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: 0,
                      translateY: 0,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget287())
                ]);
              }),
            )
          ]),
    );
  }
}
