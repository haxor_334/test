import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/generated/GeneratedLabelWidget188.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/generated/GeneratedFriesWidget5.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/generated/GeneratedBgWidget192.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentraterecipewidget/generated/GeneratedImageWidget102.dart';

/* Group Recipe 13
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe13Widget5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget192(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget188(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 43.0,
              height: 29.0,
              child: GeneratedFriesWidget5(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget102(),
            )
          ]),
    );
  }
}
