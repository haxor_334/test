import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedBgWidget46.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedImageWidget24.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedLettuceWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedLabelWidget84.dart';

/* Group Recipe 11
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe11Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget46(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget84(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 65.0,
              height: 29.0,
              child: GeneratedLettuceWidget(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget24(),
            )
          ]),
    );
  }
}
