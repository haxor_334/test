import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedLabelWidget79.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedSliceBreadWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedImageWidget19.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedBgWidget36.dart';

/* Group Recipe 3
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe3Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget36(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget79(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 93.0,
              height: 26.0,
              child: GeneratedSliceBreadWidget(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget19(),
            )
          ]),
    );
  }
}
