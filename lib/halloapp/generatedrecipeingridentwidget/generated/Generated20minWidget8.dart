import 'package:flutter/material.dart';

/* Text 20 min
    Autogenerated by FlutLab FTF Generator
  */
class Generated20minWidget8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        '''20 min''',
        overflow: TextOverflow.visible,
        textAlign: TextAlign.left,
        style: TextStyle(
          height: 1.171875,
          fontSize: 11.0,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w400,
          color: Color.fromARGB(255, 217, 217, 217),

          /* letterSpacing: 0.0, */
        ),
      ),
    );
  }
}
