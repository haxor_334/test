import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedBgWidget50.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedRedGreenChilliWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedImageWidget26.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentwidget/generated/GeneratedLabelWidget86.dart';

/* Group Recipe 5
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe5Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget50(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget86(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 147.0,
              height: 26.0,
              child: GeneratedRedGreenChilliWidget(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget26(),
            )
          ]),
    );
  }
}
