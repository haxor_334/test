import 'package:flutter/material.dart';

/* Rectangle Rectangle 6
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRectangle6Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375.0,
      height: 823.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(-5.960479498856586e-8, -1.000000000099421),
          end: Alignment(0.00266666946806704, 0.9999999548177778),
          stops: [0.0, 1.0],
          colors: [Color.fromARGB(102, 0, 0, 0), Color.fromARGB(255, 0, 0, 0)],
        ),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.zero,
        child: Image.asset(
          "assets/images/abb8bc9ad3d82b4122f5602afeadd620a4c53d15.png",
          color: null,
          fit: BoxFit.cover,
          width: 375.0,
          height: 823.0,
          colorBlendMode: BlendMode.dstATop,
        ),
      ),
    );
  }
}
