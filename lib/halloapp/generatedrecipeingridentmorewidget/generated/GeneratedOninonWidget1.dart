import 'package:flutter/material.dart';

/* Text Oninon
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedOninonWidget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Oninon''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 16.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
