import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentmorewidget/generated/GeneratedBgWidget66.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentmorewidget/generated/GeneratedImageWidget35.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentmorewidget/generated/GeneratedLabelWidget99.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentmorewidget/generated/GeneratedGreenonionWidget1.dart';

/* Group Recipe 9
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe9Widget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget66(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget99(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 101.0,
              height: 26.0,
              child: GeneratedGreenonionWidget1(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget35(),
            )
          ]),
    );
  }
}
