import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget366 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 13.200000762939453,
      height: 5.914584159851074,
      child: SvgWidget(painters: [
        SvgPathPainter.stroke(
          1.5,
          strokeCap: StrokeCap.round,
          strokeJoin: StrokeJoin.miter,
        )
          ..addPath(
              'M13.7303 0.53033C14.0232 0.237437 14.0232 -0.237437 13.7303 -0.53033C13.4374 -0.823223 12.9626 -0.823223 12.6697 -0.53033L13.7303 0.53033ZM0.53033 -0.53033C0.237437 -0.823223 -0.237437 -0.823223 -0.53033 -0.53033C-0.823223 -0.237437 -0.823223 0.237437 -0.53033 0.53033L0.53033 -0.53033ZM12.6697 -0.53033L7.23634 4.903L8.297 5.96366L13.7303 0.53033L12.6697 -0.53033ZM7.23634 4.903C6.88756 5.25178 6.31244 5.25178 5.96366 4.903L4.903 5.96366C5.83756 6.89822 7.36244 6.89822 8.297 5.96366L7.23634 4.903ZM5.96366 4.903L0.53033 -0.53033L-0.53033 0.53033L4.903 5.96366L5.96366 4.903Z')
          ..color = Color.fromARGB(255, 255, 255, 255),
      ]),
    );
  }
}
