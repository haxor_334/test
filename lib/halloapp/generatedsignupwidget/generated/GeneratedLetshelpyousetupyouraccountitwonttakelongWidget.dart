import 'package:flutter/material.dart';

/* Text Let’s help you set up your account, it won’t take long.
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedLetshelpyousetupyouraccountitwonttakelongWidget
    extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Let’s help you set up your account, it won’t take long.''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 11.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
