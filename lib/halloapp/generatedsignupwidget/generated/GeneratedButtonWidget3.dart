import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedsignupwidget/generated/GeneratedRectangle642Widget3.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatedsignupwidget/generated/GeneratedVuesaxboldfacebookWidget1.dart';

/* Group Button
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedButtonWidget3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 44.0,
      height: 44.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 44.0,
              height: 44.0,
              child: GeneratedRectangle642Widget3(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                final double width = constraints.maxWidth * 0.5454545454545454;

                final double height =
                    constraints.maxHeight * 0.5454545454545454;

                return Stack(children: [
                  TransformHelper.translate(
                      x: constraints.maxWidth * 0.22727272727272727,
                      y: constraints.maxHeight * 0.22727272727272727,
                      z: 0,
                      child: Container(
                        width: width,
                        height: height,
                        child: GeneratedVuesaxboldfacebookWidget1(),
                      ))
                ]);
              }),
            )
          ]),
    );
  }
}
