import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedStep6Widget7.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedLabelWidget207.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedBgWidget204.dart';

/* Group Step 6
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedStep6Widget6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 93.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 93.0,
              child: GeneratedBgWidget204(),
            ),
            Positioned(
              left: 15.0,
              top: 32.0,
              right: null,
              bottom: null,
              width: 287.0,
              height: 53.0,
              child: GeneratedLabelWidget207(),
            ),
            Positioned(
              left: 15.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 19.0,
              child: GeneratedStep6Widget7(),
            )
          ]),
    );
  }
}
