import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedVectorWidget454.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedVectorWidget453.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedVectorWidget452.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatedrecipeprocedurewidget/generated/GeneratedVectorWidget451.dart';

/* Group timer
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTimerWidget19 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 17.0,
      height: 17.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.7916667040656594;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 13.458333969116211;

                double percentHeight = 0.7916667601641487;
                double scaleY = (constraints.maxHeight * percentHeight) /
                    13.458334922790527;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.10416666199179257,
                      translateY: constraints.maxHeight * 0.15625,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget451())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.0625;
                double scaleX = (constraints.maxWidth * percentWidth) / 1.0625;

                double percentHeight = 0.2708333520328297;
                double scaleY = (constraints.maxHeight * percentHeight) /
                    4.6041669845581055;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.46875,
                      translateY: constraints.maxHeight * 0.3020833520328297,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget452())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.3125;
                double scaleX = (constraints.maxWidth * percentWidth) / 5.3125;

                double percentHeight = 0.0625;
                double scaleY =
                    (constraints.maxHeight * percentHeight) / 1.0625;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.34375,
                      translateY: constraints.maxHeight * 0.052083327489740705,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget453())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 1.0;
                double scaleX = (constraints.maxWidth * percentWidth) / 17.0;

                double percentHeight = 1.0;
                double scaleY = (constraints.maxHeight * percentHeight) / 17.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: 0,
                      translateY: 0,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget454())
                ]);
              }),
            )
          ]),
    );
  }
}
