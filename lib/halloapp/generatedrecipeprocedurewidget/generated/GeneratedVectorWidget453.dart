import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Vector
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedVectorWidget453 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 5.3125,
      height: 1.0625,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M4.78125 1.0625L0.53125 1.0625C0.240833 1.0625 0 0.821667 0 0.53125C0 0.240833 0.240833 0 0.53125 0L4.78125 0C5.07167 0 5.3125 0.240833 5.3125 0.53125C5.3125 0.821667 5.07167 1.0625 4.78125 1.0625Z')
          ..color = Color.fromARGB(255, 217, 217, 217),
      ]),
    );
  }
}
