import 'package:flutter/material.dart';

/* Rectangle Rectangle 39
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRectangle39Widget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.5,
      child: Container(
        width: 280.0,
        height: 43.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(9.0),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(9.0),
          child: Container(
            color: Color.fromARGB(255, 217, 217, 217),
          ),
        ),
      ),
    );
  }
}
