import 'package:flutter/material.dart';

/* Text app.Recipe.co/jollof_rice
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedAppRecipecojollof_riceWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''app.Recipe.co/jollof_rice''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 11.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w500,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
