import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedpopwidget2/generated/GeneratedFrame47Widget2.dart';

/* Group pop up
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedPopupWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 310.0,
      height: 167.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 310.0,
              height: 167.0,
              child: GeneratedFrame47Widget2(),
            )
          ]),
    );
  }
}
