import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedframe48widget/generated/GeneratedRectangle22Widget.dart';
import 'package:flutterapp/halloapp/generatedframe48widget/generated/GeneratedLinkCopiedWidget.dart';

/* Group Group 72
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedGroup72Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70.0,
      height: 22.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 70.0,
              height: 22.0,
              child: GeneratedRectangle22Widget(),
            ),
            Positioned(
              left: 11.0,
              top: 6.0,
              right: null,
              bottom: null,
              width: 50.0,
              height: 14.0,
              child: GeneratedLinkCopiedWidget(),
            )
          ]),
    );
  }
}
