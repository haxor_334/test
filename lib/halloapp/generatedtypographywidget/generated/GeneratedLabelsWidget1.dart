import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedFontsizeWidget15.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedLineheightWidget15.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedTextstyleWidget1.dart';

/* Group labels
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedLabelsWidget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 848.0,
      height: 24.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 768.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 82.0,
              height: 26.0,
              child: GeneratedLineheightWidget15(),
            ),
            Positioned(
              left: 576.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 65.0,
              height: 26.0,
              child: GeneratedFontsizeWidget15(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 71.0,
              height: 26.0,
              child: GeneratedTextstyleWidget1(),
            )
          ]),
    );
  }
}
