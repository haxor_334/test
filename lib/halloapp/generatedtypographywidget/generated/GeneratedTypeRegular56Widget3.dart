import 'package:flutter/material.dart';

/* Text Type Regular@56
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTypeRegular56Widget3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Medium Text Bold''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 18.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
