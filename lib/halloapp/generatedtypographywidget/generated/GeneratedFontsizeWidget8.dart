import 'package:flutter/material.dart';

/* Text font size
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFontsizeWidget8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''30px''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.5714285714285714,
        fontSize: 14.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        color: Color.fromARGB(255, 169, 169, 169),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
