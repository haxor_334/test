import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedFontsizeWidget11.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedTextstylenameWidget10.dart';
import 'package:flutterapp/halloapp/generatedtypographywidget/generated/GeneratedLineheightWidget11.dart';

/* Group Properties
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedPropertiesWidget10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 799.0,
      height: 22.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 767.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 27.0,
              child: GeneratedLineheightWidget11(),
            ),
            Positioned(
              left: 576.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 35.0,
              height: 27.0,
              child: GeneratedFontsizeWidget11(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 199.0,
              height: 24.0,
              child: GeneratedTextstylenameWidget10(),
            )
          ]),
    );
  }
}
