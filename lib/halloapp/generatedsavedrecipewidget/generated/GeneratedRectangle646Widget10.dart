import 'package:flutter/material.dart';

/* Rectangle Rectangle 646
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRectangle646Widget10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 150.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image.asset(
          "assets/images/2fcbd544559e1d3d17f51640b7e705c80290a38e.png",
          color: null,
          fit: BoxFit.cover,
          width: 315.0,
          height: 150.0,
          colorBlendMode: BlendMode.dstATop,
        ),
      ),
    );
  }
}
