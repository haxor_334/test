import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedsavedrecipewidget/generated/GeneratedLabelWidget13.dart';

/* Frame Tabs
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTabsWidget5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150.0,
      height: 33.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 9.0,
              top: 8.0,
              right: null,
              bottom: null,
              width: 137.0,
              height: 22.0,
              child: GeneratedLabelWidget13(),
            )
          ]),
    );
  }
}
