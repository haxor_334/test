import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/svg/svg.dart';

/* Vector Stroke 2
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedStroke2Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 14.940485954284668,
      height: 19.0,
      child: SvgWidget(painters: [
        SvgPathPainter.fill()
          ..addPath(
              'M7.47024 0C1.08324 0 0.00424319 0.932 0.00424319 8.429C0.00424319 16.822 -0.152757 19 1.44324 19C3.03824 19 5.64324 15.316 7.47024 15.316C9.29724 15.316 11.9022 19 13.4972 19C15.0932 19 14.9362 16.822 14.9362 8.429C14.9362 0.932 13.8572 0 7.47024 0Z')
          ..color = Color.fromARGB(255, 219, 235, 231),
      ]),
    );
  }
}
