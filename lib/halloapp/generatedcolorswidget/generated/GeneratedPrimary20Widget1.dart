import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedPrimary20Widget2.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedPrimary40Widget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedDBEBE7Widget.dart';

/* Group Primary/20
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedPrimary20Widget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 138.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 130.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 75.0,
              height: 27.0,
              child: GeneratedDBEBE7Widget(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 200.0,
              height: 100.0,
              child: GeneratedPrimary20Widget2(),
            ),
            Positioned(
              left: 0.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 92.0,
              height: 24.0,
              child: GeneratedPrimary40Widget(),
            )
          ]),
    );
  }
}
