import 'package:flutter/material.dart';

/* Text #31B057
    Autogenerated by FlutLab FTF Generator
  */
class Generated31B057Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''#31B057''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.right,
      style: TextStyle(
        height: 1.399999976158142,
        fontSize: 16.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
