import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedEAF7EEWidget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedSuccess10Widget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedGreen10Widget.dart';

/* Group Secondary/90
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedSecondary90Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: false,
      child: Container(
        width: 200.0,
        height: 138.0,
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              Positioned(
                left: 134.0,
                top: 116.0,
                right: null,
                bottom: null,
                width: 71.0,
                height: 27.0,
                child: GeneratedEAF7EEWidget(),
              ),
              Positioned(
                left: 0.0,
                top: 116.0,
                right: null,
                bottom: null,
                width: 89.0,
                height: 24.0,
                child: GeneratedSuccess10Widget(),
              ),
              Positioned(
                left: 0.0,
                top: 0.0,
                right: null,
                bottom: null,
                width: 200.0,
                height: 100.0,
                child: GeneratedGreen10Widget(),
              )
            ]),
      ),
    );
  }
}
