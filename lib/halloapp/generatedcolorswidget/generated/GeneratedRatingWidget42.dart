import 'package:flutter/material.dart';

/* Text Rating
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRatingWidget42 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Rating''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.5714285714285714,
        fontSize: 14.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 18, 18, 18),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
