import 'package:flutter/material.dart';

/* Rectangle Neutral/100
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedNeutral100Widget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 100.0,
      child: ClipRRect(
        borderRadius: BorderRadius.zero,
        child: Container(
          color: Color.fromARGB(255, 0, 0, 0),
        ),
      ),
    );
  }
}
