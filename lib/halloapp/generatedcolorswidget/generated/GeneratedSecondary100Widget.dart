import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedGreen100Widget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/Generated31B057Widget.dart';
import 'package:flutterapp/halloapp/generatedcolorswidget/generated/GeneratedSuccessWidget1.dart';

/* Group Secondary/100
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedSecondary100Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 138.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 133.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 72.0,
              height: 27.0,
              child: Generated31B057Widget(),
            ),
            Positioned(
              left: 0.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 69.0,
              height: 24.0,
              child: GeneratedSuccessWidget1(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 200.0,
              height: 100.0,
              child: GeneratedGreen100Widget(),
            )
          ]),
    );
  }
}
