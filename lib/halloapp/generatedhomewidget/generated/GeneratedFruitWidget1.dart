import 'package:flutter/material.dart';

/* Text Fruit
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFruitWidget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Text(
        '''Fruit''',
        overflow: TextOverflow.visible,
        textAlign: TextAlign.center,
        style: TextStyle(
          height: 1.171875,
          fontSize: 11.0,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w600,
          color: Color.fromARGB(255, 113, 177, 161),

          /* letterSpacing: 0.0, */
        ),
      ),
    );
  }
}
