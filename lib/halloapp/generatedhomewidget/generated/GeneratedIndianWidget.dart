import 'package:flutter/material.dart';

/* Text Indian
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedIndianWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        '''Indian''',
        overflow: TextOverflow.visible,
        textAlign: TextAlign.left,
        style: TextStyle(
          height: 1.171875,
          fontSize: 14.0,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w400,
          color: Color.fromARGB(255, 169, 169, 169),

          /* letterSpacing: 0.0, */
        ),
      ),
    );
  }
}
