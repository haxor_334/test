import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedEllipse1Widget9.dart';

/* Group Image
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedImageWidget11 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 86.0,
      height: 86.0,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(38, 32, 32, 32),
            offset: Offset(0.0, 6.311926364898682),
            blurRadius: 19.724769592285156,
          )
        ],
      ),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 86.0,
              height: 86.0,
              child: GeneratedEllipse1Widget9(),
            )
          ]),
    );
  }
}
