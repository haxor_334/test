import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedTimeWidget10.dart';

/* Group Time & Portion
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTimePortionWidget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      height: 39.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 40.0,
              height: 39.0,
              child: GeneratedTimeWidget10(),
            )
          ]),
    );
  }
}
