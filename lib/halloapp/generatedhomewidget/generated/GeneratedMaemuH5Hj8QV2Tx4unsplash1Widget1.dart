import 'package:flutter/material.dart';

/* Rectangle mae-mu-H5Hj8QV2Tx4-unsplash 1
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedMaemuH5Hj8QV2Tx4unsplash1Widget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 110.0,
      height: 117.0,
      child: ClipRRect(
        borderRadius: BorderRadius.zero,
        child: Image.asset(
          "assets/images/215adfd78430a33b2d32f851a7a46fbcc86ff15b.png",
          color: null,
          fit: BoxFit.cover,
          width: 110.0,
          height: 117.0,
          colorBlendMode: BlendMode.dstATop,
        ),
      ),
    );
  }
}
