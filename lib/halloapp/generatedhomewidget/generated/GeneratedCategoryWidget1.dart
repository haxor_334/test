import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedAsianWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedIndianWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedItalianWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedLocalDishWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedAllWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedChineseWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedVegetablesWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedFruitWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedCerealWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedProteinWidget.dart';

/* Frame Category 
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCategoryWidget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 697.0,
      height: 21.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 30.0,
              height: 21.0,
              child: GeneratedAllWidget(),
            ),
            Positioned(
              left: 52.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 49.0,
              height: 26.0,
              child: GeneratedIndianWidget(),
            ),
            Positioned(
              left: 118.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 49.0,
              height: 26.0,
              child: GeneratedItalianWidget(),
            ),
            Positioned(
              left: 184.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 44.0,
              height: 26.0,
              child: GeneratedAsianWidget(),
            ),
            Positioned(
              left: 245.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 62.0,
              height: 26.0,
              child: GeneratedChineseWidget(),
            ),
            Positioned(
              left: 324.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 72.0,
              height: 23.0,
              child: GeneratedLocalDishWidget(),
            ),
            Positioned(
              left: 416.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 84.0,
              height: 26.0,
              child: GeneratedVegetablesWidget(),
            ),
            Positioned(
              left: 517.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 26.0,
              child: GeneratedCerealWidget(),
            ),
            Positioned(
              left: 586.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 35.0,
              height: 26.0,
              child: GeneratedFruitWidget(),
            ),
            Positioned(
              left: 638.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 54.0,
              height: 26.0,
              child: GeneratedProteinWidget(),
            )
          ]),
    );
  }
}
