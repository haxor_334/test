import 'package:flutter/material.dart';

/* Text Time
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTimeWidget13 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Time''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 11.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w400,
        color: Color.fromARGB(255, 169, 169, 169),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
