import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedFruitWidget1.dart';

/* Frame Button
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedButtonWidget9 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 65.0,
      height: 31.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            Positioned(
              left: 20.0,
              top: 7.0,
              right: null,
              bottom: null,
              width: 30.0,
              height: 22.0,
              child: GeneratedFruitWidget1(),
            )
          ]),
    );
  }
}
