import 'package:flutter/material.dart';

/* Rectangle mae-mu-H5Hj8QV2Tx4-unsplash 1
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedMaemuH5Hj8QV2Tx4unsplash1Widget4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 138.0,
      height: 133.0,
      child: ClipRRect(
        borderRadius: BorderRadius.zero,
        child: Image.asset(
          "assets/images/c562fe72cb2d468b0c21525ed3569de28013e142.png",
          color: null,
          fit: BoxFit.cover,
          width: 138.0,
          height: 133.0,
          colorBlendMode: BlendMode.dstATop,
        ),
      ),
    );
  }
}
