import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/Generated20minsWidget3.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedVuesaxoutlinetimerWidget6.dart';

/* Frame Time
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTimeWidget17 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 65.0,
      height: 25.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 4.0,
              right: null,
              bottom: null,
              width: 17.0,
              height: 17.0,
              child: GeneratedVuesaxoutlinetimerWidget6(),
            ),
            Positioned(
              left: 22.0,
              top: 4.0,
              right: null,
              bottom: null,
              width: 45.0,
              height: 19.0,
              child: Generated20minsWidget3(),
            )
          ]),
    );
  }
}
