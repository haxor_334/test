import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/Generated15MinsWidget.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedTimeWidget5.dart';

/* Group Time
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTimeWidget4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      height: 39.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 32.0,
              height: 22.0,
              child: GeneratedTimeWidget5(),
            ),
            Positioned(
              left: 0.0,
              top: 22.0,
              right: null,
              bottom: null,
              width: 42.0,
              height: 19.0,
              child: Generated15MinsWidget(),
            )
          ]),
    );
  }
}
