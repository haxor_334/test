import 'package:flutter/material.dart';
import 'package:flutterapp/helpers/mask/mask.dart';
import 'package:flutterapp/halloapp/generatedhomewidget/generated/GeneratedMaemuH5Hj8QV2Tx4unsplash1Widget7.dart';

/* Ellipse Ellipse 1
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedEllipse1Widget10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Mask.fromSVGPath(
      'M86 43C86 66.7482 66.7482 86 43 86C19.2518 86 0 66.7482 0 43C0 19.2518 19.2518 0 43 0C66.7482 0 86 19.2518 86 43Z',
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: -14.0,
              top: -4.0,
              right: null,
              bottom: null,
              width: 117.0,
              height: 100.0,
              child: GeneratedMaemuH5Hj8QV2Tx4unsplash1Widget7(),
            )
          ]),
      offset: Offset(0.0, 0.0),
    );
  }
}
