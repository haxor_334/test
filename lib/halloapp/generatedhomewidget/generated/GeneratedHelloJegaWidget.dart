import 'package:flutter/material.dart';

/* Text Hello Jega
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedHelloJegaWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Hello Jega''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 20.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 0, 0, 0),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
