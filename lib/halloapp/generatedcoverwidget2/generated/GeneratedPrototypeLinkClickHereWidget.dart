import 'package:flutter/material.dart';

/* Text Prototype Link: Click Here
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedPrototypeLinkClickHereWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RichText(
        overflow: TextOverflow.visible,
        textAlign: TextAlign.left,
        text: const TextSpan(
          style: TextStyle(
            height: 1.171875,
            fontSize: 21.0,
            fontFamily: 'Pangram',
            fontWeight: FontWeight.w500,
            color: Color.fromARGB(255, 255, 255, 255),

            /* letterSpacing: 0.0, */
          ),
          children: [
            TextSpan(
              text: '''Prototype Link: ''',
            ),
            TextSpan(
              text: '''Click Here''',
              style: TextStyle(
                color: Color.fromARGB(255, 27, 159, 255),
                decoration: TextDecoration.underline,
                /* letterSpacing: null, */
              ),
            )
          ],
        ));
  }
}
