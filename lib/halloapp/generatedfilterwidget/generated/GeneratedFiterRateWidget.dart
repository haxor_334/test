import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedComponent1Widget.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedComponent2Widget.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedTimeWidget38.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedComponent3Widget1.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedComponent4Widget.dart';

/* Group Fiter/Rate
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFiterRateWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 256.0,
      height: 58.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedTimeWidget38(),
            ),
            Positioned(
              left: 0.0,
              top: 31.0,
              right: null,
              bottom: null,
              width: 33.0,
              height: 27.0,
              child: GeneratedComponent1Widget(),
            ),
            Positioned(
              left: 43.0,
              top: 31.0,
              right: null,
              bottom: null,
              width: 61.0,
              height: 27.0,
              child: GeneratedComponent2Widget(),
            ),
            Positioned(
              left: 114.0,
              top: 31.0,
              right: null,
              bottom: null,
              width: 56.0,
              height: 27.0,
              child: GeneratedComponent3Widget1(),
            ),
            Positioned(
              left: 180.0,
              top: 31.0,
              right: null,
              bottom: null,
              width: 76.0,
              height: 27.0,
              child: GeneratedComponent4Widget(),
            )
          ]),
    );
  }
}
