import 'package:flutter/material.dart';

/* Text Filter Search
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedFilterSearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      '''Filter Search''',
      overflow: TextOverflow.visible,
      textAlign: TextAlign.left,
      style: TextStyle(
        height: 1.171875,
        fontSize: 14.0,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Color.fromARGB(255, 0, 0, 0),

        /* letterSpacing: 0.0, */
      ),
    );
  }
}
