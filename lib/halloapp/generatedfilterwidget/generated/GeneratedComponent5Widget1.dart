import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedVuesaxlinearstarWidget28.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/GeneratedStar5Widget12.dart';
import 'package:flutterapp/halloapp/generatedfilterwidget/generated/Generated5Widget12.dart';

/* Instance Component 5
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedComponent5Widget1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 83.0,
      height: 27.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(
          width: 1.0,
          color: Color.fromARGB(255, 113, 177, 161),
        ),
      ),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Container(
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ),
            Positioned(
              left: 10.0,
              top: 5.0,
              right: null,
              bottom: null,
              width: 68.0,
              height: 22.0,
              child: Generated5Widget12(),
            ),
            Positioned(
              left: 24.0,
              top: 6.5,
              right: null,
              bottom: null,
              width: 18.0,
              height: 18.0,
              child: GeneratedVuesaxlinearstarWidget28(),
            ),
            Positioned(
              left: 22.0,
              top: 5.0,
              right: null,
              bottom: null,
              width: 18.0,
              height: 18.0,
              child: GeneratedStar5Widget12(),
            )
          ]),
    );
  }
}
