import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedTimeWidget51.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedFoodtitleWidget33.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedImageWidget60.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedRectangle644Widget27.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedRatingWidget37.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedBookmarkWidget16.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharelinkcopiedwidget/generated/GeneratedReviewsWidget3.dart';

/* Group Card
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCardWidget32 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 201.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedImageWidget60(),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 150.0,
              child: GeneratedRectangle644Widget27(),
            ),
            Positioned(
              left: 268.0,
              top: 10.0,
              right: null,
              bottom: null,
              width: 37.0,
              height: 16.0,
              child: GeneratedRatingWidget37(),
            ),
            Positioned(
              left: 5.0,
              top: 160.0,
              right: null,
              bottom: null,
              width: 196.0,
              height: 43.0,
              child: GeneratedFoodtitleWidget33(),
            ),
            Positioned(
              left: 284.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 24.0,
              height: 24.0,
              child: GeneratedBookmarkWidget16(),
            ),
            Positioned(
              left: 214.0,
              top: 116.0,
              right: null,
              bottom: null,
              width: 60.0,
              height: 24.0,
              child: GeneratedTimeWidget51(),
            ),
            Positioned(
              left: 217.0,
              top: 160.0,
              right: null,
              bottom: null,
              width: 93.0,
              height: 20.0,
              child: GeneratedReviewsWidget3(),
            )
          ]),
    );
  }
}
