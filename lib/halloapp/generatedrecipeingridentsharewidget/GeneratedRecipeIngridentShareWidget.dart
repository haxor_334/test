import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedFrame5Widget2.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedStatusBarWidget14.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedTimeWidget49.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedComponent3Widget5.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedPopWidget.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedCreatorsProfileWidget2.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedRectangle654Widget1.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedHomeIndicatorWidget15.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedTimeWidget48.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedEllipse1Widget23.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedCardWidget31.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedComponent4Widget5.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedHeaderWidget11.dart';

/* Frame Recipe/Ingrident/Share
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipeIngridentShareWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: ClipRRect(
      borderRadius: BorderRadius.circular(30.0),
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Container(
              width: 375.0,
              height: 812.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.center,
                  overflow: Overflow.visible,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(30.0),
                      child: Container(
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ),
                    Positioned(
                      left: 0.0,
                      top: -43.0,
                      right: null,
                      bottom: null,
                      width: 308.0,
                      height: 106.0,
                      child: GeneratedEllipse1Widget23(),
                    ),
                    Positioned(
                      left: 0.0,
                      top: 790.0,
                      right: null,
                      bottom: null,
                      width: 375.0,
                      height: 34.0,
                      child: GeneratedHomeIndicatorWidget15(),
                    ),
                    Positioned(
                      left: 30.0,
                      top: 88.0,
                      right: null,
                      bottom: null,
                      width: 315.0,
                      height: 201.0,
                      child: GeneratedCardWidget31(),
                    ),
                    Positioned(
                      left: 30.0,
                      top: 427.0,
                      right: null,
                      bottom: null,
                      width: 59.0,
                      height: 24.0,
                      child: GeneratedTimeWidget48(),
                    ),
                    Positioned(
                      left: 301.0,
                      top: 427.0,
                      right: null,
                      bottom: null,
                      width: 44.0,
                      height: 24.0,
                      child: GeneratedTimeWidget49(),
                    ),
                    Positioned(
                      left: 30.0,
                      top: 299.0,
                      right: null,
                      bottom: null,
                      width: 315.0,
                      height: 40.0,
                      child: GeneratedCreatorsProfileWidget2(),
                    ),
                    Positioned(
                      left: 0.0,
                      top: 359.0,
                      right: null,
                      bottom: null,
                      width: 375.0,
                      height: 58.0,
                      child: GeneratedComponent3Widget5(),
                    ),
                    Positioned(
                      left: 30.0,
                      top: 471.0,
                      right: null,
                      bottom: null,
                      width: 315.0,
                      height: 334.0,
                      child: GeneratedFrame5Widget2(),
                    ),
                    Positioned(
                      left: 162.0,
                      top: 769.0,
                      right: null,
                      bottom: null,
                      width: 50.0,
                      height: 24.0,
                      child: GeneratedComponent4Widget5(),
                    ),
                    Positioned(
                      left: 0.0,
                      top: 0.0,
                      right: null,
                      bottom: null,
                      width: 375.0,
                      height: 812.0,
                      child: GeneratedRectangle654Widget1(),
                    ),
                    Positioned(
                      left: 30.0,
                      top: 322.0,
                      right: null,
                      bottom: null,
                      width: 315.0,
                      height: 167.0,
                      child: GeneratedPopWidget(),
                    )
                  ]),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: null,
              width: null,
              height: 44.0,
              child: GeneratedStatusBarWidget14(),
            ),
            Positioned(
              left: 30.0,
              top: 54.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 24.0,
              child: GeneratedHeaderWidget11(),
            )
          ]),
    ));
  }
}
