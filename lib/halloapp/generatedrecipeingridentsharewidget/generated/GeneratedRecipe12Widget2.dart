import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedBgWidget104.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedSpinachWidget2.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedImageWidget55.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedLabelWidget127.dart';

/* Group Recipe 12
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRecipe12Widget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 315.0,
      height: 76.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 315.0,
              height: 76.0,
              child: GeneratedBgWidget104(),
            ),
            Positioned(
              left: 264.0,
              top: 28.0,
              right: null,
              bottom: null,
              width: 41.0,
              height: 26.0,
              child: GeneratedLabelWidget127(),
            ),
            Positioned(
              left: 83.0,
              top: 29.0,
              right: null,
              bottom: null,
              width: 72.0,
              height: 29.0,
              child: GeneratedSpinachWidget2(),
            ),
            Positioned(
              left: 15.0,
              top: 12.0,
              right: null,
              bottom: null,
              width: 52.0,
              height: 52.0,
              child: GeneratedImageWidget55(),
            )
          ]),
    );
  }
}
