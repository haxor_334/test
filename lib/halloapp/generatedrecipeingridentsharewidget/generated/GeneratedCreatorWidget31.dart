import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedUnsplashIj24Uq1sMwMWidget7.dart';
import 'package:flutterapp/halloapp/generatedrecipeingridentsharewidget/generated/GeneratedInfoWidget2.dart';

/* Group Creator
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedCreatorWidget31 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 145.0,
      height: 40.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 40.0,
              height: 40.0,
              child: GeneratedUnsplashIj24Uq1sMwMWidget7(),
            ),
            Positioned(
              left: 50.0,
              top: 0.0,
              right: null,
              bottom: null,
              width: 95.0,
              height: 40.0,
              child: GeneratedInfoWidget2(),
            )
          ]),
    );
  }
}
