import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatednotificationsallwidget/generated/GeneratedLabelWidget16.dart';

/* Frame Tabs
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedTabsWidget9 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Navigator.pushNamed(context, '/GeneratedNotificationsReadWidget'),
      child: Container(
        width: 107.0,
        height: 33.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              Positioned(
                left: 12.0,
                top: 8.0,
                right: null,
                bottom: null,
                width: 88.0,
                height: 22.0,
                child: GeneratedLabelWidget16(),
              )
            ]),
      ),
    );
  }
}
