import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/generated/GeneratedVectorWidget266.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/generated/GeneratedVectorWidget267.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/generated/GeneratedVectorWidget268.dart';

/* Group notification-bing
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedNotificationbingWidget6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 24.0,
      height: 24.0,
      child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: [
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.7135381698608398;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 17.124916076660156;

                double percentHeight = 0.7062500317891439;
                double scaleY = (constraints.maxHeight * percentHeight) /
                    16.950000762939453;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.14334278305371603,
                      translateY: constraints.maxHeight * 0.08541666467984517,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget266())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 0.23541665077209473;
                double scaleX =
                    (constraints.maxWidth * percentWidth) / 5.649999618530273;

                double percentHeight = 0.08333333333333333;
                double scaleY = (constraints.maxHeight * percentHeight) / 2.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 0.38250001271565753,
                      translateY: constraints.maxHeight * 0.8333333333333334,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget267())
                ]);
              }),
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              right: 0.0,
              bottom: 0.0,
              width: null,
              height: null,
              child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                double percentWidth = 1.0;
                double scaleX = (constraints.maxWidth * percentWidth) / 24.0;

                double percentHeight = 1.0;
                double scaleY = (constraints.maxHeight * percentHeight) / 24.0;

                return Stack(children: [
                  TransformHelper.translateAndScale(
                      translateX: constraints.maxWidth * 1.0,
                      translateY: constraints.maxHeight * 1.0,
                      translateZ: 0,
                      scaleX: scaleX,
                      scaleY: scaleY,
                      scaleZ: 1,
                      child: GeneratedVectorWidget268())
                ]);
              }),
            )
          ]),
    );
  }
}
