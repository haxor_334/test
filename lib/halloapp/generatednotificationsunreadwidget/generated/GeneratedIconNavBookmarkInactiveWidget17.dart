import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/generated/GeneratedUnionWidget28.dart';
import 'package:flutterapp/helpers/transform/transform.dart';
import 'package:flutterapp/halloapp/generatednotificationsunreadwidget/generated/GeneratedRectangle1Widget30.dart';

/* Instance Icon/Nav/Bookmark/Inactive
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedIconNavBookmarkInactiveWidget17 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/GeneratedSavedRecipeWidget'),
      child: Container(
        width: 24.0,
        height: 24.0,
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              Positioned(
                left: 0.0,
                top: 0.0,
                right: 0.0,
                bottom: 0.0,
                width: null,
                height: null,
                child: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  final double width = constraints.maxWidth;

                  final double height = constraints.maxHeight;

                  return Stack(children: [
                    TransformHelper.translate(
                        x: 0,
                        y: 0,
                        z: 0,
                        child: Container(
                          width: width,
                          height: height,
                          child: GeneratedRectangle1Widget30(),
                        ))
                  ]);
                }),
              ),
              Positioned(
                left: 0.0,
                top: 0.0,
                right: 0.0,
                bottom: 0.0,
                width: null,
                height: null,
                child: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints constraints) {
                  double percentWidth = 0.6850308577219645;
                  double scaleX =
                      (constraints.maxWidth * percentWidth) / 16.44074058532715;

                  double percentHeight = 0.8541666666666666;
                  double scaleY =
                      (constraints.maxHeight * percentHeight) / 20.5;

                  return Stack(children: [
                    TransformHelper.translateAndScale(
                        translateX: constraints.maxWidth * 0.15624491373697916,
                        translateY: constraints.maxHeight * 0.07291666666666667,
                        translateZ: 0,
                        scaleX: scaleX,
                        scaleY: scaleY,
                        scaleZ: 1,
                        child: GeneratedUnionWidget28())
                  ]);
                }),
              )
            ]),
      ),
    );
  }
}
