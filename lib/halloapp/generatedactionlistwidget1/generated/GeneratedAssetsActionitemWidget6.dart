import 'package:flutter/material.dart';
import 'package:flutterapp/halloapp/generatedactionlistwidget1/generated/GeneratedLabelWidget195.dart';
import 'package:flutterapp/halloapp/generatedactionlistwidget1/generated/GeneratedInputfieldvuesaxboldmessageWidget1.dart';

/* Instance Assets/Action item
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedAssetsActionitemWidget6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Navigator.pushNamed(context, '/GeneratedNotificationsReadWidget1'),
      child: Container(
        width: 144.0,
        height: 40.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              Positioned(
                left: 44.0,
                top: 10.0,
                right: 46.0,
                bottom: null,
                width: null,
                height: 26.0,
                child: GeneratedLabelWidget195(),
              ),
              Positioned(
                left: 8.0,
                top: 10.0,
                right: null,
                bottom: null,
                width: 20.0,
                height: 20.0,
                child: GeneratedInputfieldvuesaxboldmessageWidget1(),
              )
            ]),
      ),
    );
  }
}
