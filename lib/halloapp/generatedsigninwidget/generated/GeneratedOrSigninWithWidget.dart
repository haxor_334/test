import 'package:flutter/material.dart';

/* Text Or Sign in With
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedOrSigninWithWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        '''Or Sign in With''',
        overflow: TextOverflow.visible,
        textAlign: TextAlign.left,
        style: TextStyle(
          height: 1.171875,
          fontSize: 11.0,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w500,
          color: Color.fromARGB(255, 217, 217, 217),

          /* letterSpacing: 0.0, */
        ),
      ),
    );
  }
}
