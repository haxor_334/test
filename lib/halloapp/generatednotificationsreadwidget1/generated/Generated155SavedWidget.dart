import 'package:flutter/material.dart';

/* Text 155 Saved
    Autogenerated by FlutLab FTF Generator
  */
class Generated155SavedWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        '''155 Saved''',
        overflow: TextOverflow.visible,
        textAlign: TextAlign.left,
        style: TextStyle(
          height: 1.171875,
          fontSize: 11.0,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w400,
          color: Color.fromARGB(255, 169, 169, 169),

          /* letterSpacing: 0.0, */
        ),
      ),
    );
  }
}
