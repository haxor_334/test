import 'package:flutter/material.dart';

/* Rectangle Rectangle 646
    Autogenerated by FlutLab FTF Generator
  */
class GeneratedRectangle646Widget2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150.0,
      height: 150.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image.asset(
          "assets/images/94a5dab432e7c160269453dc8f5072b693f1d1d3.png",
          color: null,
          fit: BoxFit.cover,
          width: 150.0,
          height: 150.0,
          colorBlendMode: BlendMode.dstATop,
        ),
      ),
    );
  }
}
